#include <benchmark/benchmark.h>
#include "Knapsack_problem/tests.h"
#include "Hotel_problem/tests.h"

static void knapsack_problem (benchmark::State& state) {
    for (auto _ : state) {
        benchmark::DoNotOptimize(RandomGenerator(state.range(0)));
    }
    //state.SetItemsProcessed(state.range(0));
    //state.SetBytesProcessed(state.range(0));
    //state.SetComplexityN(state.range(0));
}

static void knapsack_dynamic (benchmark::State& state) {
    for (auto _ : state) {
        benchmark::DoNotOptimize(DynGen(state.range(0)));
    }
    //state.SetItemsProcessed(state.range(0));
    //state.SetBytesProcessed(state.range(0));
    //state.SetComplexityN(state.range(0));
}

static void hotel_problem (benchmark::State& state) {
    for (auto _ : state) {
        benchmark::DoNotOptimize(RGWR_HOTEL(state.range(0)));
    }
    //state.SetItemsProcessed(state.range(0));
    //state.SetBytesProcessed(state.range(0));
    //state.SetComplexityN(state.range(0));
}

BENCHMARK(knapsack_problem)->DenseRange(1, 100, 10)->Unit(benchmark::kMillisecond);
//BENCHMARK(knapsack_dynamic)->DenseRange(1, 100, 10)->Unit(benchmark::kMillisecond);
//BENCHMARK(hotel_problem)->DenseRange(1, 20, 1)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();