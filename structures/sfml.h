#pragma once
#include <SFML/Graphics.hpp>
#include "vector"

#include "Knapsack_problem/Item.h"
#include "Knapsack_problem/Knapsack.h"

bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp)
{
    if (sprite.contains(mp)){
        return true;
    }
    return false;
}

sf::RectangleShape* RectCreate(float size){
    auto *rect  = new sf::RectangleShape(sf::Vector2f(size, size));
    rect->setFillColor(sf::Color::Transparent);
    rect->setOutlineThickness(3.);
    rect->setOutlineColor(sf::Color::White);
    return rect;
}

int* draw_shape(int width, int height){
    int modeWidth = 600;
    int modeHeight = 400;
    sf::RenderWindow window(sf::VideoMode(modeWidth, modeHeight), "Draw figure");
    int size = 50;

    int* my_shape = new int[width*height]{0};
    std::vector<sf::RectangleShape> rects;

    sf::Vector2f mp;
    mp.x = sf::Mouse::getPosition(window).x;
    mp.y = sf::Mouse::getPosition(window).y;

    for (int i=0; i<height; ++i) {
        for (int j=0; j<width; ++j) {
            auto *rect = RectCreate(size);
            rect->setPosition(j*size + 20, i*size + 20);
            rects.push_back(*rect);
        }
    }

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        for (int i=0; i<height; i++){
            for (int j=0; j<width; j++){
                auto k = i*width + j;
                if(isSpriteHover(rects[k].getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y))) {
                    if (event.type == sf::Event::MouseButtonPressed) {
                        if (rects[k].getFillColor().toInteger() == sf::Color::Transparent.toInteger()) {
                            rects[k].setFillColor(sf::Color::Cyan);
                            my_shape[i*width + j] = 1;
                        }
                      //  else {
                      //      rects[k].setFillColor(sf::Color::Transparent);
                      //      my_shape[i*width + j] = 0;
                      //  }
                    }
                }
            }
        }

        window.clear();
        for (auto const& s: rects) { window.draw(s); }
        window.display();
    }

    window.close();
    return my_shape;
}


void draw_item(Item *item){
    int modeWidth = 600;
    int modeHeight = 400;
    sf::RenderWindow window(sf::VideoMode(modeWidth, modeHeight), "Figure");
    int size = 50;
    std::vector<sf::RectangleShape> rects;
    for (int i=0; i<item->height; ++i) {
        for (int j=0; j<item->width; ++j) {
            if (item->shape[i*item->width + j]) {
                auto *rect = RectCreate(size);
                rect->setPosition(j * size + 20, i * size + 20);
                rect->setFillColor(sf::Color::Blue);
                rects.push_back(*rect);
            }
        }
    }

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        for (auto const& s: rects) { window.draw(s); }
        window.display();
    }

    window.close();
}

void Knapsack::draw_backpack(const Knapsack *kn) {
    int modeWidth = 600;
    int modeHeight = 400;
    sf::RenderWindow window(sf::VideoMode(modeWidth, modeHeight), "Backpack");
    int size = 50;
    std::vector<sf::RectangleShape> rects;

    for (int i=0; i<kn->height; ++i) {
        for (int j=0; j<kn->width; ++j) {
            auto *rect = RectCreate(size);
            rect->setPosition(j * size + 20, i * size + 20);
            rects.push_back(*rect);
        }
    }

    std::vector<sf::Color> colors {sf::Color::Green, sf::Color::Cyan, sf::Color::Blue, sf::Color::Magenta,
                                   sf::Color::Yellow, sf::Color::White};

    int j=0;
    for (auto &figure: kn->figures){
        for (int i=0; i<kn->width*kn->height; i++){
            if (figure[i]) {
                rects[i].setFillColor(colors[j % colors.size()]);
            }
        }
        j++;
    }

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        for (int i=0; i<kn->width*kn->height; i++){
            if (kn->bp_init_shape[i]) {
                window.draw(rects[i]);
            }
        }
        window.display();
    }

    window.close();
}