#pragma once
#include <vector>

using namespace std;

template <class T> class Set;
template <class T> class Node_Iterator;

template<class T>
struct Node_Set
{
  Node_Set() {_left = nullptr; _right = nullptr; _friend = nullptr;}
  explicit Node_Set(const T& data) : Node_Set() {_value = data;}

  ~Node_Set()
  {
    delete _left;
    delete _right;
  }

  T _value;
  Node_Set *_left;
  Node_Set *_right;
  Node_Set *_friend; // in-order traversal
};

template<class T>
class Set
{
public :
  Set() :
          _size{0}, _root{nullptr}, startIterator{nullptr}
  {}

  Set(std::initializer_list<T> l) : Set() {
    for (auto &el: l){
      insert(el);
    }
  }

  ~Set()
  {
    clear();
  }

  typedef Node_Iterator<T> iterator;

  Set(const Set &s)
  {
    _size = 0;
    _root = nullptr;
    for(iterator it = s.begin(); it != s.end(); it++)
    {
      insert(*it);
    }
  }

  Set& operator =(const Set &s)
  {
    if(&s == this) return *this;

    clear();
    for(iterator it = s.begin(); it != s.end(); it++)
    {
      insert(*it);
    }
    return *this;
  }

  void add(Set &s){
    for(iterator it = s.begin(); it != s.end(); it++)
    {
      insert(*it);
    }
  }

  void clear()
  {
    if(_root)
    {
      delete _root;
      _root = nullptr;
      _size = 0;
    }
  }

  size_t size() const
  {
    return _size;
  }

  bool empty() const
  {
    return _size == 0;
  }

  mutable size_t _size{};
  mutable Node_Set<T> *_root;
  mutable Node_Set<T> *startIterator;

  T& insert(const T& t)
  {
    return insert_imp(t, _root, _size);
  }

  bool find(const T& t)
  {
    return (find_imp(t,_root) != nullptr);
  }

  bool remove(const T& t)
  {
    Node_Set<T> **nodeRemove = find_imp(t,_root);

    if(nodeRemove == nullptr) {
      return false;
    }

    _size--;
    remove_imp(*nodeRemove);
    return true;
  }

  iterator begin() const
  {
    build_iterators(_root);
    return iterator(startIterator);
  }

  iterator end() const {
    return iterator(0);
  }

  T first() const {
    return _root->_value;
  }

private :
  void build_iterators(const Node_Set<T> *root) const
  {
    size_t i;
    std::vector<Node_Set<T>*> iterators;
    build_iterators_imp(iterators, root);

    if(empty())
    {
      startIterator = nullptr;
      return;
    }

    startIterator = iterators[0];
    for(i = 0; i < iterators.size() - 1; i++)
    {
      iterators[i]->_friend = iterators[i + 1];
    }
    iterators[i]->_friend = nullptr;
  }

  static void build_iterators_imp(std::vector<Node_Set<T>*> &iterators, const Node_Set<T> *root)
  {
    if(root == nullptr) {
      return;
    }

    build_iterators_imp(iterators, root->_left);

    iterators.push_back((Node_Set<T>*)root);

    build_iterators_imp(iterators, root->_right);
  }

  static T& insert_imp(const T& t, Node_Set<T> *&root, size_t &_size)
  {
    if(!root)
    {
      _size++;
      root = new Node_Set<T>(t);
      return root->_value;
    }

    if(t == root->_value)
    {
      return root->_value;
    }
    else if(t < root->_value)
    {
      return insert_imp(t, root->_left, _size);
    }
    else
    {
      return insert_imp(t, root->_right, _size);
    }
  }

  static Node_Set<T>** find_imp(const T& t, Node_Set<T> *&root)
  {
    if(!root)
    {
      return nullptr;
    }

    if(t == root->_value)
    {
      return &root;
    }
    else if(t < root->_value)
    {
      return find_imp(t,root->_left);
    }
    else
    {
      return find_imp(t,root->_right);
    }
  }

  static void travel_rightmost(Node_Set<T> *&_this, Node_Set<T> **&nodeResult)
  {
    if(!_this) return;
    if(_this->_right) {
      nodeResult = &_this;
      return travel_rightmost(_this->_right, nodeResult);
    }
  }

  static void travel_leftmost(Node_Set<T> *&_this, Node_Set<T> **&nodeResult)
  {
    if(!_this) return;
    if(_this->_left) {
      nodeResult = &_this;
      return travel_leftmost(_this->_left, nodeResult);
    }
  }

  static void remove_imp(Node_Set<T> *&root)
  {
    if(root == nullptr) {
      return;
    }

    if(root->_left == nullptr) {
      if (root->_right == nullptr) {
        delete root;
        root = nullptr;
      } else {
        Node_Set<T> *rightNode = root->_right;
        root->_right = nullptr;
        delete root;
        root = rightNode;
      }
    } else {
      if (root->_right == nullptr) {
        Node_Set<T> *leftNode = root->_left;
        root->_left = nullptr;
        delete root;
        root = leftNode;
      } else {
        Node_Set<T> **left = &root->_right;
        while ((*left)->_left) {
          left = &(*left)->_left;
        }
        root->_value = (*left)->_value;
        remove_imp(*left);
      }
    }
  }
};

template <class T>
class Node_Iterator
{
  Node_Set<T> * n;
public:
  Node_Iterator() : n(0) {}
  explicit Node_Iterator(Node_Set<T> * newNode) : n(newNode) { }

  bool operator == (Node_Iterator it) const { return n == it.n; }
  bool operator != (Node_Iterator it) const { return n != it.n; }

  // In-order traversal
  Node_Iterator & operator++();
  const Node_Iterator operator++(int);

  T & operator*() { return n->_value; }
  T * operator->() { return &n->_value; }
  Node_Iterator & operator=(Node_Iterator<T> it) { n = it.n; return *this; }

  friend class Set<T>;
};

template <class T>
Node_Iterator<T>& Node_Iterator<T>::operator ++()
{
  n = n->_friend;
  return *this;
}

template <class T>
const Node_Iterator<T> Node_Iterator<T>::operator ++(int)
{
  Node_Iterator<T> it(*this); ++(*this);
  return it;
}