#pragma once
#include "vector"

template <typename T, typename U>
class Dictionary{
private:
  using v = std::vector<U*>;
  using value_type = std::pair<T*, v>;
  std::vector<value_type> entries;

public:
  Dictionary();
  Dictionary(Dictionary<T,U> &dictionary);
  bool has(T*) const;
  void add(T*, U*);
  void add(T*, v);
  void merge(Dictionary<T,U> &dictionary);
  value_type* begin();
  value_type* end();
  v& operator[](T*);

  int size = 0;
  int GetCount();
  void Remove(T* Key);
  void print();
};

template <typename T, typename U>
Dictionary<T,U>::Dictionary(){
  size = 0;
}

template <typename T, typename U>
Dictionary<T,U>::Dictionary (Dictionary<T, U> &dictionary) : size(dictionary.size), entries(dictionary.entries) {}

template <typename T, typename U>
std::pair<T*, std::vector<U*>> *Dictionary<T,U>::begin(){
  return (!size) ? &entries[0] : nullptr;
}

template <typename T, typename U>
std::pair<T*, std::vector<U*>> *Dictionary<T,U>::end(){
  return (!size) ? &(entries[entries.size()-1]) : nullptr;
}

template <typename T, typename U>
bool Dictionary<T,U>::has(T* key) const{
    return std::find_if(entries.begin(), entries.end(), [&](value_type const &ref) {return ref.first == key;}) != entries.end();
}

template <typename T, typename U>
void Dictionary<T,U>::add (T* key, U* value){
  if (has(key)){
    this->operator[](key).push_back(value);
  }
  else{
    entries.push_back(value_type(key, {value}));
    size++;
  }
}

template <typename T, typename U>
void Dictionary<T,U>::add (T* key, std::vector<U*> vector){
    auto it = std::find_if(entries.begin(), entries.end(), [&](value_type const &ref) {return ref.first == key;});
    if (it != entries.end()){
        (*it).second.merge(vector);
    } else {
      entries.push_back(value_type(key, vector));
      size++;
    }
}

template <typename T, typename U>
std::vector<U*> &Dictionary<T,U>::operator[] (T* key){
    return std::find_if(entries.begin(), entries.end(), [&](value_type const &ref) {return ref.first == key;})->second;
}

template<typename T, typename U>
int Dictionary<T, U>::GetCount(){
  return size;
}

template<typename T, typename U>
void Dictionary<T, U>::Remove(T* key){
  entries.erase(std::find_if(entries.begin(), entries.end(), [&](value_type const &ref) {return ref.first == key;}));
  size--;
}

template<typename T, typename U>
void Dictionary<T, U>::print() {
  for (auto &p: entries){
    std::cout << *p.first;
    for (auto &u: p.second){
      std::cout << *u;
    }
  }
}

template<typename T, typename U>
void Dictionary<T,U>::merge(Dictionary<T, U> &dictionary) {
  for (auto &e: dictionary.entries){
    entries.push_back(e);
    size++;
  }
}