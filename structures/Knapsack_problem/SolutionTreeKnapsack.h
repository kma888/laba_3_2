#pragma once
#include "iostream"
#include "vector"

#include "Item.h"
#include "Knapsack.h"

////////////////////////////////////////////////  BACKPACK PLACEMENT  /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

[[maybe_unused]] void PrintArray(int height, int width, float * arr){
    std::cout << "\n";
    for (int i = 0; i<height; i++){
        for (int j=0; j<width; j++){
            std::cout << arr[i*width + j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

//Поворот фигуры на 90 градусов
int* rotate90(int width, int height, const int* shape){
    int *new_shape = new int[width*height]{0};
    int k=0;
    for (int j = 0; j < width; j++){
        for (int i = height - 1; i >= 0; i--){
            new_shape[k++] = shape[i*width + j];
        }
    }

    return new_shape;
}

//Сравнение двух массивов (для предотвращения повторов с симметричными фигурами)
bool comp2arr(const int* shape1, const int* shape2, int volume){
    for(int i=0; i<volume; i++){
        if (shape1[i]!=shape2[i]){
            return false;
        }
    }
    return true;
}

//Сравнение форм рюкзака и фигуры
Knapsack* compare(Knapsack &kn, int* shape){

    for (int i=0; i<kn.width*kn.height; i++){
        if (shape[i] > kn.bp_shape[i]){
            return nullptr;
        }
    }

    auto *newKN = new Knapsack(kn);
    for (int i=0; i<kn.width*kn.height; i++){
        newKN->bp_shape[i] -= shape[i];
    }

    newKN->figures.push_back(shape);

    return newKN;
}

//Все возможные расположения фигуры в рюкзаке
std::vector<Knapsack*> main_position_function(Knapsack &kn, int itemH, int itemW, const int* itemS){

    std::vector<Knapsack*> kns;

    int height = kn.height;
    int width = kn.width;
    int volume = height*width;

    int *a = new int[volume]();
    int level_difference = height - itemH;
    int width_difference = width - itemW;

    for (int j=0; j<itemH; j++){
        for (int i=0; i<itemW; i++){
            a[j*width + i] = itemS[j*itemW+i];
        }
    }

    for (int u=0; u<=level_difference; u++){
        for (int k=0; k<=width_difference; k++){
            int *b = new int[volume]();
            for (int i=0; i<volume-k; i++){
                 b[i+k] = a[i];
            }
            auto ptr = compare(kn, b);
            if (ptr) {
                kns.push_back(ptr);
            }
        }

        int *d = new int[volume]();
        for (int i=0; i< volume - width; i++){
            d[i + width] = a[i];
        }
        a = d;
    }

    return kns;
}

//Делает все возможные повороты фигуры
//Проверка на то, что фигура симметричная
std::vector<Knapsack*> positions(Knapsack &kn, Item *item){
    auto width = item->width;
    auto height = item->height;
    auto shape = item->shape;
    auto volume = item->volume;
    std::vector<Knapsack*> v1, v2, v3, v4;
    int *rot90, *rot180, *rot270;
    rot90 = rotate90(width, height, shape);
    rot180 = rotate90(height, width, rot90);
    rot270 = rotate90(width, height, rot180);

    v1 = main_position_function(kn, height, width, shape);

    if (!comp2arr(shape, rot180, volume)){
        v2 = main_position_function(kn, height, width, rot180);
    }

    if (height <= kn.width && width <= kn.height) {
        if (height == width) {
            if (!comp2arr(shape, rot90, volume)) {
                v3 = main_position_function(kn, width, height, rot90);
            }
            if (!comp2arr(shape, rot270, volume) && !comp2arr(rot90, rot270, volume)){
                v4 = main_position_function(kn, width, height, rot270);
            }
        }

        else {
            v3 = main_position_function(kn, width, height, rot90);
            if (!comp2arr(rot90, rot270, volume)) {
                v4 = main_position_function(kn, width, height, rot270);
            }
        }
    }

    v1.insert(v1.end(), v2.begin(), v2.end());
    v3.insert(v3.end(), v4.begin(), v4.end());
    v1.insert(v1.end(), v3.begin(), v3.end());

    return v1;
}

///////////////////////////////////////////////   SOLUTION TREE   /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename T, typename I, typename K>
//T – float, I – Item, K - Knapsack
class tree {
private:
    struct node {
        T* value;
        bool leaf = false;

        node() {
            value = new T();
        };

        explicit node(T v) : node() {*value = v;}

        explicit node(node* nd) : node(*nd->value){
            for (auto &kid: nd->kids){
                kids.push_back(kid);
            }
            for (auto &k: nd->ItemSet){
                ItemSet.push_back(k);
            }
        }

        std::vector<node *> kids;
        std::vector<K *> ItemSet;

        node *last() {
            return kids.back();
        }

        void add(node *_node) {
            kids.push_back(_node);
        }

        void print() {
            std::cout << "VALUE: " << *value << "\n";
            for (auto &kn: ItemSet) {
                kn->print();
                std::cout << "\n";
            }
        }
    };

public:
    node *head;

    tree() {
        head = new node;
    }

    void print() {
        head->print();
        std::cout << "\n";
        for (auto const &s: head->kids) {
            print(s);
        }
    }

    void print(node *_node) {
        if (!_node) { return; }
        _node->print();
        std::cout << "\n";
        for (auto const &s: _node->kids) { print(s); }
    }

    void clear() {
        for (auto const &s: head->kids) {
            clear(s);
        }
        delete head;
    }

    void clear(node *_node) {
        for (auto const &s: _node->kids) {
            clear(s);
        }
        delete _node;
    }

    typedef typename std::vector<I*>::iterator iterator;
    //Рюкзак и предметы не имеют формы
    std::vector<node *>
    SolutionHelper(node *_node, std::vector<I*> &objs, K &bp, std::initializer_list<int K::*> const &kP,
                   std::vector<int I::*> const &iP, int I::*FP, iterator it) {
        std::vector<node *> max_nodes = {_node};
        auto max_value = *_node->value;

        while (it!= objs.end()){
            bool flag = true;

            auto kIter = kP.begin();
            auto iIter = iP.begin();

            while (kIter!=kP.end()){
                if (bp.*(*kIter) < *it->*(*iIter)){
                    flag = false;
                    break;
                }
                kIter++;
                iIter++;
            }

            if (flag) {
                K *bp2 = new K(bp);
                kIter = kP.begin();
                iIter = iP.begin();

                while (kIter!=kP.end()){
                    bp2->*(*kIter) -= *it->*(*iIter);
                    kIter++;
                    iIter++;
                }

                bp2->items.push_back(*it);
                node *_new = new node(*_node->value + *it->*FP);
                _node->add(_new);
                for (auto &r : SolutionHelper(_node->last(), objs, *bp2, kP, iP, FP, std::next(it))) {
                    if (*r->value >= max_value) {
                        if (*r->value > max_value) {
                            max_nodes.clear();
                        }
                        if (r->leaf) { max_nodes.push_back(r); }
                        max_value = *r->value;
                    }
                }
            }
            it++;
        }

        //Терминальный узел
        if (max_nodes[0] == _node) {
            _node->ItemSet.push_back(&bp);
            _node->leaf = true;
        }

        return max_nodes;
    }

    //Рюкзак и предметы имеют форму
    std::vector<node *>
    SolutionHelperWithShape(node *_node, std::vector<I*> &objs, K &bp, std::initializer_list<int Knapsack::*> const &kP,
                            std::initializer_list<int Item::*> const &iP, int I::*FP, iterator it) {

        std::vector<node *> max_nodes = {_node};
        auto max_value = *_node->value;

        while (it!=objs.end()) {
            auto *bp2 = new K(bp);
            bool flag = true;

            auto kIter = kP.begin();
            auto iIter = iP.begin();

            while (kIter!=kP.end()){
                if (bp.*(*kIter) < *it->*(*iIter)){
                    flag = false;
                    break;
                }
                bp2->*(*kIter) -= *it->*(*iIter);
                kIter++;
                iIter++;
            }

            if (flag) {
                bp2->items.push_back(*it);
                node *_new = new node(*_node->value + *it->*FP);
                _node->add(_new);
                for (auto &bp3: positions(*bp2, *it)) {
                    for (auto &r: SolutionHelperWithShape(_node->last(), objs, *bp3, kP, iP, FP, std::next(it))) {
                        if (*r->value >= max_value) {
                            if (*r->value > max_value) {
                                max_nodes.clear();
                            }
                            if (*std::find(max_nodes.begin(), max_nodes.end(), r) != r) {
                                max_nodes.push_back(r);
                            }
                            max_value = *r->value;
                        }
                    }
                }
            }
            it++;
        }

        //Терминальный узел
        if (max_nodes[0] == _node){
            _node->ItemSet.push_back(&bp);
        }

        return max_nodes;
    }
};

typedef tree<int, Item, Knapsack> mTree;

void SOLUTION(std::vector<Item*> &objs, Knapsack &bp, std::initializer_list<int Knapsack::*> const &kP,
              std::initializer_list<int Item::*> const &iP, int Item::*FP){
    mTree Solution = mTree();
    //Если предметы имеют форму сортируем по объёму
    if (objs[0]->shape) {
        auto sortRuleLambda = [](Item *s1, Item *s2) -> bool {
            return s1->volume > s2->volume;
        };
        std::sort(objs.begin(), objs.end(), sortRuleLambda);
        std::cout << "\nДерево решений:\n";
        for (auto &node: Solution.SolutionHelperWithShape(Solution.head, objs, bp, kP, iP, FP, objs.begin())){
            node->print();
        }
    }

    else{
        std::cout << "\nДерево решений:\n";
        for (auto &node: Solution.SolutionHelper(Solution.head, objs, bp, kP, iP, FP, objs.begin())){
            node->print();
        }
    }
}