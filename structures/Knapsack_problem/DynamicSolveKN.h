#pragma once
#include "iostream"
#include "vector"

#include "Item.h"
#include "Knapsack.h"

int DynamicSolution(std::vector<Item*> &items, Knapsack &kn, std::initializer_list<int Knapsack::*> const &kP,
                    std::initializer_list<int Item::*> const &iP, int Item::*FP){
    unsigned long N = items.size();
    unsigned long W = kn.max_weight;
    int d[N+1][W+1];

    for (unsigned long i=0; i <= N; i++) {
        for (unsigned long j=0; j <= W; j++) {
            if (i==0 || j==0)
                d[i][j] = 0;
            else if (items[i-1]->weight <= j) {
                d[i][j] = std::max(d[i - 1][j], d[i - 1][j - items[i - 1]->weight] + items[i - 1]->price);
            }
            else
                d[i][j] = d[i-1][j];
        }
    }
    std::cout << "Динамическое программирование\n" << "Value\t" << d[N][W] << "\n";
    std::vector<Item*> ans;

    for (unsigned long i = N; i > 0; i--){
        if (d[i][W]!=d[i-1][W]) {
            ans.push_back(items[i-1]);
            W -= items[i-1]->weight;
        }
    }

    for (auto & item: ans){
        item->print();
    }

    return d[N][W];
}

int DynamicSolution2(std::vector<Item*> &items, Knapsack &kn, std::initializer_list<int Knapsack::*> const &kP,
                     std::initializer_list<int Item::*> const &iP, int Item::*FP){
    unsigned long N = items.size();
    unsigned long W = kn.max_weight;
    unsigned long V = kn.max_volume;
    int d[N+1][W+1][V+1];

    for (unsigned long i=0; i <= N; i++) {
        for (unsigned long j=0; j <= W; j++) {
            for (unsigned long k=0; k <= V; k++) {
                if (i == 0 || j == 0 || k == 0)
                    d[i][j][k] = 0;
                else if (items[i - 1]->weight <= j && items[i-1]->volume <= k) {
                    d[i][j][k] = std::max(d[i - 1][j - items[i-1]->weight][k-items[i-1]->volume] + items[i-1]->price,
                                          d[i - 1][j][k]);
                } else
                    d[i][j][k] = d[i - 1][j][k];
            }
        }
    }  

    std::cout << "Динамическое программирование\n" << "Value\t" << d[N][W][V] << "\n";
    std::vector<Item*> ans;
    auto W1 = W;
    auto V1 = V;

   for (unsigned long i = N; i > 0; i--){
       if (d[i][W][V]!=d[i-1][W][V]) {
           ans.push_back(items[i-1]);
           W -= items[i-1]->weight;
           V -= items[i-1]->volume;
       }
   }

    for (auto & item: ans){
        item->print();
    }

    return d[N][W1][V1];
}