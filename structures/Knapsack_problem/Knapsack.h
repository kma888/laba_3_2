#pragma once
#include "iostream"
#include "Item.h"

struct Knapsack{

    int max_weight = 0;
    int width = 0;
    int height = 0;
    int max_volume = 0;
    int *bp_shape = nullptr;
    const int *bp_init_shape = nullptr;
    std::vector<Item*> items;
    std::vector<int*> figures;
    static void draw_backpack(const Knapsack* kn) ;

    Knapsack() = default;
    explicit Knapsack(int w) : max_weight(w) {};
    explicit Knapsack(int w, int v) : max_weight(w), max_volume(v) {}
    explicit Knapsack(int w, int wi, int h, int* s) : max_weight(w), width(wi), height(h), bp_init_shape(s) {
        bp_shape = new int[wi*h];
        for (int i=0; i<width*height; i++) {
            bp_shape[i] = s[i];
            if (s[i]) {max_volume++;}
        }
    }
    Knapsack(Knapsack &kn) :
            max_weight(kn.max_weight), width(kn.width), height(kn.height), max_volume(kn.max_volume), bp_init_shape(kn.bp_init_shape)
    {
        items = kn.items;
        figures = kn.figures;
        if (kn.bp_shape) {
            bp_shape = new int[width*height];
            for (int i=0; i<width*height; i++) {
                bp_shape[i] = kn.bp_shape[i];
            }
        }
    }
    ~Knapsack() = default;

    void print() const {
        if (!items.empty()){
            for (auto &item: items){
                item->print();
            }
            if (bp_init_shape){
                draw_backpack(this);
            }
        }
        else {
            std::cout << "Рюкзак:\n";
            std::cout << "Вес:  " << max_weight << "\n";
            if (max_volume > 0) {
                std::cout << "Объём:  " << max_volume << "\n";
            }
            if (bp_init_shape){
                draw_backpack(this);
            }
        }
    }
    friend std::ostream& operator<< (std::ostream &out, const Knapsack &kn){
        out << "Weight:  " << kn.max_weight << "\n";
        if (kn.max_volume) {out << "Volume: " << kn.max_volume << "\n";}
        return out;
    }
    bool operator==(const Knapsack&) const = default;
};
