#pragma once
#include "vector"

#include "Item.h"
#include "Knapsack.h"
#include "SolutionTreeKnapsack.h"
#include "sfml.h"
#include "DynamicSolveKN.h"

Knapsack &KnapsackWithShape() {

    int bp_max_weight, bp_width, bp_height;

    std::cout << "Введите максимальный вес рюкзака: ";
    std::cin >> bp_max_weight;
    std::cout << "Введите максимальную ширину рюкзака: ";
    std::cin >> bp_width;
    std::cout << "Введите максимальную высоту рюкзака: ";
    std::cin >> bp_height;
    std::cout << "Нарисуйте рюкзак: ";

    auto *bp = new Knapsack(bp_max_weight, bp_width, bp_height, draw_shape(bp_width, bp_height));
    return *bp;
}

std::vector<Item*> &ItemsWithShape(){
    int width1, height1, weight, price;

    auto *my_objs = new std::vector<Item*>;
    int obj_num;
    std::cout << "Введите кол-во фигур: ";
    std::cin >> obj_num;

    for (int i=1; i<=obj_num; ++i){
        std::cout << "Введите цену объекта № " << i << " ";
        std::cin >> price;
        std::cout << "Введите вес объекта № " << i << " ";
        std::cin >> weight;
        std::cout << "Введите ширину и высоту фигуры ";
        std::cin >> width1 >> height1;
        std::cout << "Нарисуйте фигуру № " << i << "\n";

        Item *item = new Item(price, weight, width1, height1, draw_shape(width1, height1));
        my_objs->push_back(item);
    }

    return *my_objs;
}

Knapsack &KnapsackNoShape(){
    int bp_max_volume, bp_max_weight;

    std::cout << "Введите максимальный вес рюкзака: ";
    std::cin >> bp_max_weight;
    std::cout << "Введите максимальный объём рюкзака: ";
    std::cin >> bp_max_volume;

    auto *bp = new Knapsack(bp_max_weight, bp_max_volume);
    return *bp;
}

std::vector<Item*> &ItemsNoShape(){

    auto *my_objs = new std::vector<Item*>;
    int obj_num;
    std::cout << "Введите кол-во фигур: ";
    std::cin >> obj_num;

    int volume, weight, price;

    for (int i=1; i<=obj_num; ++i){
        std::cout << "Введите цену объекта № " << i << " ";
        std::cin >> price;
        std::cout << "Введите вес объекта № " << i << " ";
        std::cin >> weight;
        std::cout << "Введите объём предмета № " << i << " ";
        std::cin >> volume;

        Item *item = new Item(price, weight, volume);
        my_objs->push_back(item);
    }

    return *my_objs;
}

////////////////////////////////////////////// RANDOM GENERATION //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int RandomGenerator(int n){
    assert(n>0);
    Knapsack genKN(rand()%(2*n) + 1, rand()%(2*n) + 1);
    std::vector<Item*> genItems;

    for (int i=0; i<n; i++){
        Item *genItem = new Item(rand()%n + 1,rand()%n + 1, rand()%n + 1);
        genItems.push_back(genItem);
    }

    for (auto &item: genItems){
        item->print();
    }
    genKN.print();

    SOLUTION(genItems, genKN, {&Knapsack::max_weight, &Knapsack::max_volume},
             {&Item::weight, &Item::volume}, &Item::price);
    return 0;
}

int DynGen(long long n) {
    assert(n > 0);
    Knapsack genKN(rand() % (2 * n) + 1, rand() % (2 * n) + 1);
    std::vector<Item *> genItems;

    for (int i = 0; i < n; i++) {
        Item *genItem = new Item(rand() % n + 1, rand() % n + 1, rand() % n + 1);
        genItems.push_back(genItem);
    }

    for (auto &item: genItems){
        item->print();
    }
    genKN.print();

    return DynamicSolution2(genItems, genKN, {&Knapsack::max_weight, &Knapsack::max_volume},
                           {&Item::weight, &Item::volume}, &Item::price);
}

/////////////////////////////////////////////////////// TESTS /////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void run_simple_tests() {

    std::cout << "Тесты";

    Knapsack k = Knapsack(8);
    Knapsack k_ = Knapsack(9, 5);

    Item i1 = Item(2, 2);
    Item i2 = Item(5, 6);
    Item i3 = Item(4, 2);
    Item i4 = Item(3, 4);

    Item i_1 = Item(2, 2, 2);
    Item i_2 = Item(2, 3, 3);
    Item i_3 = Item(1, 1, 4);
    Item i_4 = Item(3, 4, 1);

    std::vector<Item *> items = {&i1, &i2, &i3, &i4};
    std::vector<Item *> items_ = {&i_1, &i_2, &i_3, &i_4};

    std::cout << "\n\n\nТест №1\n" << "Входные данные:\n";
    k.print();
    for (auto &obj : items){obj->print();}
    SOLUTION(items, k, {&Knapsack::max_weight}, {&Item::weight}, &Item::price);
    DynamicSolution(items, k, {&Knapsack::max_weight}, {&Item::weight}, &Item::price);

    std::cout << "\n\n\nТест №2\n" << "Входные данные:\n";
    k_.print();
    for (auto &obj : items_){obj->print();}
    SOLUTION(items_, k_, {&Knapsack::max_weight, &Knapsack::max_volume},{&Item::weight, &Item::volume}, &Item::price);
    DynamicSolution2(items_, k_, {&Knapsack::max_weight, &Knapsack::max_volume},{&Item::weight, &Item::volume}, &Item::price);
}

void run_sfml_tests() {

    std::cout << "\nТест №8";
    int z[10] = {1, 1, 1, 1, 0, 1, 1, 1, 1, 1};
    int n[6] = {0, 1, 0, 1, 1, 1};
    int m[6] = {1, 1, 0, 0, 1, 1};
    int k[1] = {1};
    Item item1(1., 1, 1, 1, k);
    Item item2(2., 2, 3, 2, m);
    Item item3(3., 3, 3, 2, n);
    std::vector<Item *> objs{&item1, &item2, &item3};
    Knapsack kn(6., 5, 2, z);
    for (auto &i: objs){
      draw_item(i);
    }
    kn.print();
    SOLUTION(objs, kn, {&Knapsack::max_weight, &Knapsack::max_volume}, {&Item::weight, &Item::volume}, &Item::price);


    std::cout << "\nТест №9";
    int C1[12] = {1,1,0,1,1,0,0,1,1,1,1,1};
    int C2[3] = {1,1};
    int C3[9] = {0,1,1,0,1,0,1,1,0};
    int C4[16] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
    Item i1(1,1,4,3,C1);
    Item i2(1,1,1,2,C2);
    Item i3(1,1,3,3,C3);
    std::vector<Item *> Objs{&i1, &i2, &i3};
    for (auto &i: Objs){
        draw_item(i);
    }
    Knapsack bp(10, 4, 4, C4);
    SOLUTION(Objs, bp, {&Knapsack::max_weight, &Knapsack::max_volume}, {&Item::weight, &Item::volume}, &Item::price);


    std::cout << "\nТест №10";
    int D1[6] = {1,1,1,1,1,1};
    int D2[4] = {1,1,1,0};
    int D3[4] = {1,1,1,1};
    int D4[8] = {1,1,1,0,1,1,1,1};
    Item I1(2,2,3,2,D1);
    Item I2(1,1,2,2,D2);
    Item I3(1,1,2,2,D3);
    std::vector<Item *> bjs{&I1, &I2, &I3};
    for (auto &i: bjs){
        draw_item(i);
    }
    Knapsack Bp(2, 4, 2, D4);
    SOLUTION(bjs, Bp, {&Knapsack::max_weight, &Knapsack::max_volume}, {&Item::weight, &Item::volume}, &Item::price);
}