#pragma once
#include "iostream"

struct Item{

    int price = 0;
    int weight = 0;
    int width = 0;
    int height = 0;
    int volume = 0;
    int* shape = nullptr;

    Item() = default;
    explicit Item(int p, int w) : price(p), weight(w) {}
    explicit Item(int p, int w, int v) : price(p), weight(w), volume(v) {}
    explicit Item(int p, int w, int wi, int he, int* s) : price(p), weight(w), width(wi), height(he), shape(s) {
        for (int i=0; i<wi*he; i++) { if (s[i]) {volume++;} }
    }
    Item(Item &item) : price(item.price), weight(item.weight), volume(item.volume), shape(item.shape) {}
    ~Item() = default;
    void print() const{
        std::cout << "Предмет:\n";
        std::cout << "Price:  " << price << "\t" << "Weight:  " << weight << "\t";
        if (volume > 0) {std::cout << "Volume:  " << volume << "\t";}
        std::cout << "\n";
    }
    bool operator==(const Item&) const = default;

};

