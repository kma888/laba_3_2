#pragma once
#include <iostream>
using namespace std;

template<class T> class QP_Iter;

template<class A>
class QueueP
{
public:
    A* arr;
    size_t* P; // массив приоритетов
    size_t count; // количество элементов в очереди

public:
    typedef QP_Iter<A> iterator;
    // конструктора
    // конструктор без параметров
    QueueP() { count = 0; P = nullptr;}

    // конструктор копирования
    QueueP(const QueueP& Q)
    {
        arr = new A[Q.count];
        P = new size_t[Q.count];

        // скопировать _Q => *this
        count = Q.count;

        // поэлементное копирование данных
        for (size_t i = 0; i < count; i++)
            arr[i] = Q.arr[i];

        for (size_t i = 0; i < count; i++)
            P[i] = Q.P[i];
    }

    // деструктор
    ~QueueP()
    {
        if (count > 0)
        {
            delete[] arr;
            delete[] P;
        }
    }

    // оператор копирования - чтобы избежать побитового копирования
    QueueP& operator=(const QueueP& Q)
    {
        // 1. Освободить память, которая была перед этим выделена для текущего объекта
        if (count > 0)
        {
            delete[] arr;
            delete[] P;
            count = 0;
        }

        arr = new A[Q.count];
        P = new size_t[Q.count];

        // 3. Если память выделена, то копирование *this <= _Q
        count = Q.count;

        // 4. Цикл копирования данных
        for (size_t i = 0; i < count; i++)
        {
            arr[i] = Q.arr[i];
            P[i] = Q.P[i];
        }
        return *this;
    }

    A get(size_t index) const{
        return arr[index];
    }

    A Pop()
    {
        // 1. Проверка
        if (count == 0)
            return 0;

        // 2. Объявить временные массивы
        A* arr2;
        size_t* P2;

        arr2 = new A[count - 1]; // на 1 элемент меньше
        P2 = new size_t[count - 1];

        // 4. Запомнить первый элемент
        A item;
        item = arr[0];

        // 5. Скопировать данные из массивов arr=>arr2, P=>P2 без первого элемента
        for (size_t i = 0; i < count - 1; i++)
        {
            arr2[i] = arr[i + 1];
            P2[i] = P[i + 1];
        }

        // 6. Освободить предварительно выделенную память для arr, P
        if (count > 0)
        {
            delete[] arr;
            delete[] P;
        }

        // 7. Поправить количество элементов в очереди
        count--;

        // 8. Перенаправить указатели arr->arr2, P->P2
        arr = arr2;
        P = P2;

        // 9. Вернуть первый элемент очереди
        return item;
    }

    // добавляет в очередь новый элемент item с приоритетом priority
    void Push(A item, size_t priority)
    {
        if (priority==0) {
            return;
        }
        // 1. Создать новый массив-очередь и массив-приоритет
        A* arr2;
        size_t* P2;

        arr2 = (A*)new A[count + 1];
        P2 = (size_t*)new size_t[count + 1];


        // 2. Поиск позиции pos в массиве P согласно с приоритетом priority
        size_t pos;

        if (count == 0)
            pos = 0;
        else
        {
            pos = 0;
            while (pos < count)
            {
                if (P[pos] < priority) break;
                pos++;
            }
        }

        // 3. Копирование данных arr2<=arr; P2<=P
        // по формуле P = [0,1,...] + pos+1 + [pos+2,pos+3,...]
        // 3.1. Индексы массива с номерами 0..pos
        for (size_t i = 0; i < pos; i++)
        {
            arr2[i] = arr[i];
            P2[i] = P[i];
        }

        // 3.2. Добавить элемент в позиции pos
        arr2[pos] = item;
        P2[pos] = priority;

        // 3.3. Элементы после позиции pos
        for (size_t i = pos + 1; i < count + 1; i++)
        {
            arr2[i] = arr[i - 1];
            P2[i] = P[i - 1];
        }

        // 4. Освободить память, предварительно выделенную для arr и P
        if (count > 0)
        {
            delete[] arr;
            delete[] P;
        }

        // 5. Перенаправить указатели arr->arr2, P->P2
        arr = arr2;
        P = P2;

        // 6. Увеличить количество элементов в очереди на 1
        count++;
    }

    // очистка очереди
    void Clear()
    {
        if (count > 0)
        {
            delete[] arr;
            delete[] P;
            count = 0;
        }
    }

    // возвращает количество элементов в очереди
    size_t Count()
    {
        return count;
    }

    // функция, выводящая очередь
    void Print()
    {
        for (size_t i = 0; i < count; i++)
            cout << arr[i] << ":" << P[i] << "\t" << endl;
        cout << "---------------" << endl;
    }

    iterator begin() const {
        return QP_Iter<A>(arr);
    }

    iterator end() const {
        assert (count > 0);
        return QP_Iter<A>(arr+count);
    }
};

/*
Наивная
В качестве наивной реализации мы можем взять обычный список и при добавлении нового элемента класть
его в конец, а при запросе элемента с максимальным приоритетом проходить по всему списку.
Тогда операция insert будет выполняться за O(1), а extractMin или extractMax за O(n).

Обычная
Для лучшей производительности приоритетные очереди реализуют с помощью куч, что позволяет выполнять
операции вставки и удаления за O(logn). Использование специальных куч, таких как Фибоначчиева куча и
спаренная куча, позволяет еще больше улучшить асимптотику некоторый операций.


CДЕЛАТЬ НА ОСНОВЕ БИНАРНОЙ КУЧИ
*/

template<class T>
class QP_Iter
{
    T* cur;
public:
    explicit QP_Iter(T* first) : cur(first)
    {}

    bool operator == (const QP_Iter& it) const { return cur == it.cur; }
    bool operator != (const QP_Iter& it) const { return cur != it.cur; }

    void operator++() {cur++;}
    void operator--() {cur--;}

    T* operator++(int) {return cur++;}
    T* operator--(int) {return cur--;}
    T* operator*() { return cur;}

    friend class QueueP<T>;
};
