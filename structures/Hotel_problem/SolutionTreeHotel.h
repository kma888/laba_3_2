#pragma once
#include "iostream"
#include "vector"

#include "Room.h"
#include "Person.h"
#include "IPriorityQueue.h"
#include "IDictionary.h"

/////////////////////////////////////////////////  PROPERTIES  ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Возможные ограничения для расселения гостей
enum class PROPERTY1 : int {MARRIED, FRIENDS, GENDER, ENEMIES};

typedef Dictionary<Room, Person> dict;
typedef typename std::vector<Room*>::iterator RoomIter;
typedef typename std::vector<Person*>::iterator PersonIter;

//0 Заселение в номер невозможно
//2 Заселение в номер возможно + node->priority++ (для друзей)
int Comparator(RoomIter rit, PersonIter pit, dict &d, QueueP<PROPERTY1>& q) {

    auto ppl = d[*rit];
    int flag = 1;
    if ((*rit)->capacity == ppl.size()){
        return 0;
    }

    for (auto property: q) {
        switch (*property) {
            //Обязательное условие расселения
            case PROPERTY1::MARRIED:
            {
                if ((*pit)->souse) {
                    if ((*rit)->capacity >= 2) {
                        return (ppl[0] == (*pit)->souse);
                    }
                    return 0;
                }
                break;
            }
            //Обязательное ограничение
            case PROPERTY1::GENDER: {
                if (ppl[0]->gender != (*pit)->gender) {
                    return 0;
                }
                break;
            }
            //Обязательное ограничение
            case PROPERTY1::ENEMIES:
            {
                if (!(*pit)->enemies.empty()) {
                    auto enemies = (*pit)->enemies;
                    for (auto &rm: ppl) {
                        if (*std::find(enemies.begin(), enemies.end(), rm) == rm) {
                            return 0;
                        }
                    }
                }
                break;
            }
            //Желательное условие расселения
            //Если в комнате живут друзья, то это прибавляет приоритет
            case PROPERTY1::FRIENDS:
            {
                if (!(*pit)->friends.empty()) {
                    flag = 2;
                    auto friends = (*pit)->friends;
                    for (auto &rm: ppl) {
                        if (*std::find(friends.begin(), friends.end(), rm) != rm) {
                            flag = 1;
                        }
                    }
                }
                break;
            }
        }
    }
    return flag;
}

///////////////////////////////////////////////   SOLUTION TREE   /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename T, typename P, typename R>
//T – int, P – Person, R - Room
class tree1 {
private:
    typedef Dictionary<R, P> myD;
    struct node {
        T* value;
        int priority = 0;
        bool leaf = false;

        node() {
            value = new T();
        }
        explicit node(T v) : node() {
            *value = v;
        }
        explicit node(T v, myD d) : node(v) {
            D = myD(d);
        }

        std::vector<node *> kids;
        Dictionary<R, P> D;

        void add(node *n) {
            kids.push_back(n);
        }

        void print() {
            if (this) {
                std::cout << "\nVALUE: " << *value << "\n";
                D.print();
                std::cout << '\n';
            }
            else {
                std::cout << "No Solution\n\n";
            }
        }
    };

public:
    node *head;

    tree1() {
        head = new node();
    }

    void print() {
        head->print();
        std::cout << "\n";
        for (auto const &s: head->kids) {
            print(s);
        }
    }

    void print(node *n) {
        if (!n) { return; }
        n->print();
        std::cout << "\n";
        for (auto const &s: n->kids) { print(s); }
    }

    void clear() {
        for (auto const &s: head->kids) {
            clear(s);
        }
        delete head;
    }

    void clear(node *n) {
        for (auto const &s: n->kids) {
            clear(s);
        }
        delete n;
    }

    node* SolutionHelper(node *n, std::vector<P*> &people, std::vector<R*> &rooms, QueueP<PROPERTY1> &Q1, int R::*FP, PersonIter pit) {

        if (pit==people.end()){
            n->leaf = true;
            return n;
        }

        node *maxN = nullptr;
        T max_value = std::numeric_limits<T>::max();
        int mPrio = n->priority;

        for (auto rit = rooms.begin(); rit != rooms.end(); rit++){

            bool has = n->D.has(*rit);
            auto C = (has) ? Comparator(rit, pit, n->D, Q1) : 1;

            if (C == 0) {
                continue;
            }

            node* nN = (has) ? new node(*n->value, n->D) : new node(*n->value + (*rit)->*FP, n->D);
            nN->D.add(*rit, *pit);

            if (C == 2){
                ++nN->priority;
            }

            auto t = SolutionHelper(nN, people, rooms, Q1, FP, std::next(pit));
            if (t) {
                //Если у узла приоритет больше, то выбираем его
                //Если у узлов одинаковые приоритеты, выбираем наименьшей стоимости
                if (t->priority >= mPrio) {
                    if (t->priority > mPrio) {
                        mPrio = t->priority;
                        maxN = t;
                    }
                    else if (t->leaf && *t->value < max_value) {
                        max_value = *t->value;
                        maxN = t;
                    }
                }
            }
        }
        return maxN;
    }
};

//Создание очереди с приоритетами
//В порядке возрастания
QueueP<PROPERTY1> &PRIORITIES(vector<PROPERTY1> const &myQ){
    //MARRIED, GENDER, ENEMIES, FRIENDS
    int counter = 0;
    auto *Q1 = new QueueP<PROPERTY1>;
    for (auto p : myQ){
        Q1->Push(p,++counter);
    }
    return *Q1;
}

typedef tree1<int, Person, Room> mTree1;

void Solution(std::vector<Person *> &people, std::vector<Room *> &rooms, vector<PROPERTY1> const &myQ) {
    std::cout << "\nВходные данные\n";
    for (auto &p: people) {
        p->print();
    }

    for (auto &r: rooms) {
        r->print();
    }
    std::cout << "Решение:";
    auto Solution = mTree1();
    Solution.SolutionHelper(Solution.head, people, rooms, PRIORITIES(myQ), &Room::price, people.begin())->print();
}


int SWR_Hotel(std::vector<Person *> &people, std::vector<Room *> &rooms, vector<PROPERTY1> const &myQ){
    auto Solution = mTree1();
    Solution.SolutionHelper(Solution.head, people, rooms, PRIORITIES(myQ), &Room::price, people.begin());
    return 0;
}