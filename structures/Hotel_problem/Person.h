#pragma once
#include <utility>
#include "string"
#include "vector"

struct Person{
public:
    char gender{};
    std::string name;
    Person *souse = nullptr;
    std::vector<Person*> friends;
    std::vector<Person*> enemies;

public:
    Person() = default;
    Person(char g, std::string n) : gender(g), name(std::move(n)) {}
    Person(char g, char* n) : gender(g), name(std::string(n)) {}
    ~Person() = default;
    void print() const {
        std::cout << name << '\t' << gender << '\n';
        if (souse){
            std::cout << "Souse: " << souse->name << '\n';
        }
        if (!friends.empty()){
            std::cout << "Friends: ";
            for (auto &f: friends){
                std::cout << f->name << '\t';
            }
            std::cout << '\n';
        }
        if (!enemies.empty()){
            std::cout << "Enemies: ";
            for (auto &e: enemies){
                std::cout << e->name << '\t';
            }
            std::cout << '\n';
        }

    }
    friend std::ostream& operator<< (std::ostream &out, const Person &person){
        out << person.name << "\t" << person.gender << '\n';
        return out;
    }
};

