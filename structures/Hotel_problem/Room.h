#pragma once
#include <utility>

#include "string"
#include "iostream"

struct Room{
public:
    std::string number;
    int capacity = 0;
    int price = 0;

public:
    Room() = default;
    Room(std::string n, int c, int p) : number(std::move(n)), capacity(c), price(p) {}
    void print() const {
        std::cout << "Room number: " << number << "\tPrice: " << price << "\tCapacity: " << capacity << "\n";
    }
    friend std::ostream& operator<< (std::ostream &out, const Room &room){
        out << room.number << "\n";
        return out;
    }
    bool operator==(const Room&) const = default;
};
