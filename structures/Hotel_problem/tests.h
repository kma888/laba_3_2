#include "iostream"
#include "string"
#include <fstream>
#include <sstream>

#include "Person.h"
#include "Room.h"
#include "SolutionTreeHotel.h"
#include "vector"

void tests(){

    Person Marie('f', "Maria");
    Person Anya('f', "Anya");
    Person Nastya('f', "Nastya");
    Person Lena('f', "Lena");

    Person Ivan('m', "Ivan");
    Person James('m', "James");
    Person Ron('m', "Ron");
    Person Petr('m', "Petr");
    Person Maxim('m', "Maxim");
    Person Nick('m', "Nick");

    Ivan.souse=&Marie; Marie.souse=&Ivan;
    Maxim.souse=&Lena; Lena.souse=&Maxim;

    Nick.friends.push_back(&James); James.friends.push_back(&Nick);
    Petr.enemies.push_back(&Ron); Ron.enemies.push_back(&Petr);

    //Базовый тест
    std::vector<Person*> people0{&Petr, &James};
    //Тест на врагов
    std::vector<Person*> people1{&Petr, &Ron};
    //Тест на друзей
    std::vector<Person*> people2{&Nick, &James};
    //Тест на супругов
    std::vector<Person*> people3{&Ivan, &Marie};
    //Тест на гендер
    std::vector<Person*> people4{&Petr, &Nastya};

    Room a100("A100", 1, 100);
    Room a101("A101", 1, 101);
    Room a200("A200", 2, 200);
    Room a201("A201", 2, 201);
    Room a202("A202", 2, 202);
    Room a300("A300", 3, 300);
    Room a301("A301", 3, 301);

    std::vector<Room*> rooms_ {&a200};
    std::vector<Room*> rooms0 {&a100, &a101, &a200};
    std::vector<Room*> rooms2 {&a100, &a101, &a300};
    std::vector<Room*> rooms3 {&a300, &a200};

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Вариант без решения
    std::cout << "Тест №0\n";
    Solution(people1, rooms_, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Базовый тест на оптимальное расселение
    std::cout << "Тест №1\n";
    Solution(people0, rooms0, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Проверка на расселение врагов
    std::cout << "Тест №2\n";
    Solution(people1, rooms0, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Проверка на приоритет у друзей
    std::cout << "Тест №3\n";
    Solution(people2, rooms2, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Проверка на оптимальный приоритет у друзей
    std::cout << "Тест №4\n";
    Solution(people2, rooms3, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Проверка на заселение супругов
    std::cout << "Тест №5\n";
    Solution(people3, rooms0, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});

    //Проверка на гендер
    std::cout << "Тест №6\n";
    Solution(people4, rooms0, {PROPERTY1::FRIENDS, PROPERTY1::ENEMIES, PROPERTY1::GENDER, PROPERTY1::MARRIED});
}

std::vector<Person*> PeopleGen(size_t size){
    std::vector<Person*> people;
    std::string my_string;

    for (int i=0; i<size; i++){
        char *name = new char[10];
        long g = random() % 2;
        if (!g) {
            std::fstream in = std::fstream("/Users/mihail/Downloads/male_names.txt");
            auto r = random() % 2943;
            for (int j = 0 ; j < r ; j++){
                in.ignore(20, '\n');
            }
            in.getline(name, 10);
        }
        else {
            std::fstream in = std::fstream("/Users/mihail/Downloads/female_names.txt");
            auto r = random() % 5001;
            for (int j = 0 ; j < r ; j++){
                in.ignore(20, '\n');
            }
            in.getline(name, 10);
        }
        auto* P = new Person( !g ? 'm' : 'f', name);
        people.push_back(P);
    }

    return people;
}

std::vector<Room*> RoomGen(size_t size){
    std::vector<Room*> rooms;
    long int counter1=0, counter2=0, counter3=0, counter4=0, sum=0;
    while (sum <= size){
        auto r = random()%4 + 1;
        sum += r;
        switch (r) {
            case 1: {
                auto room = new Room(std::to_string(r * 100 + counter1), 1, rand() % 1000);
                rooms.push_back(room);
                counter1++;
                break;
            }
            case 2: {
                auto room = new Room(std::to_string(r * 100 + counter2), 2, rand() % 1000);
                rooms.push_back(room);
                counter2++;
                break;
            }
            case 3: {
                auto room = new Room(std::to_string(r * 100 + counter3), 3, rand() % 1000);
                rooms.push_back(room);
                counter3++;
                break;
            }
            case 4: {
                auto room = new Room(std::to_string(r * 100 + counter4), 4, rand() % 1000);
                rooms.push_back(room);
                counter4++;
                break;
            }
            default:
                break;
        }
    }
    return rooms;
}


void RandomGeneration(size_t size) {

    auto people = PeopleGen(size);
    auto rooms = RoomGen(size);

    Solution(people, rooms, {PROPERTY1::GENDER});
}

int RGWR_HOTEL(size_t size) {

    auto people = PeopleGen(size);
    auto rooms = RoomGen(size);

    return SWR_Hotel(people, rooms, {PROPERTY1::GENDER});
}