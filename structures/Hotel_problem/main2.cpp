#include "tests.h"

int main()
{
    char a;
    std::cout << "Run tests? (y/n)\n";
    std::cin >> a;

    switch(a){
        case 'y':
            tests();
    }

    char b;
    std::cout << "Запустить случайную генерацию? (y/n)\n";
    std::cin >> b;

    clock_t start;

    switch (b) {
        case 'y':
            int c;
            std::cout << "Введите кол-во человек  ";
            std::cin >> c;
            start = clock();
            RandomGeneration(c);
    }

    clock_t end = clock();
    double seconds = (double)(end - start) / CLOCKS_PER_SEC;
    printf("The time: %f seconds\n", seconds);
}